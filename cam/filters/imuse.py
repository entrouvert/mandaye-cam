# -*- coding: utf-8 -*-

from urlparse import parse_qs

from mandaye.server import get_response
from mandaye.response import template_response, _302

def associate(env, values, request, response):
    qs = parse_qs(env['QUERY_STRING'])
    if qs.has_key('type'):
        values['type'] = qs['type'][0]
    else:
        values['type'] = None
    return template_response('imuse/associate.html', values)

def is_user_locally_logged_in(env, request, response):
    session = env['beaker.session']
    if session.has_key('locally_logged_in') and session['locally_logged_in']:
        return True
    return False

def is_locally_logged_in(env, values, request, response):
    session = env['beaker.session']
    if response.msg and response and 'Quitter' in response.msg:
        session['locally_logged_in'] = True
    else:
        session['locally_logged_in'] = False
    session.save()

