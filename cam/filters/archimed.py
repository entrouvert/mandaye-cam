# -*- coding: utf-8 -*-

import copy
import json
import re
import urllib

from urlparse import parse_qs

from mandaye import config
from mandaye.backends.default import Association
from mandaye.http import HTTPResponse, HTTPHeader, HTTPRequest
from mandaye.log import logger
from mandaye.response import _500, _302, template_response
from mandaye.server import get_response
from mandaye.template import serve_template

def default_req(env, values, request):
    # Disable forward headers
    request.headers.delheader('X-Forwarded-For')
    request.headers.delheader('X-Forwarded-Host')
    if  request.cookies.has_key('S_HTTP_REFERER'):
        request.cookies['S_HTTP_REFERER'] = re.sub(r'\?SAMLRequest=.*$',
                '',
                str(request.cookies['S_HTTP_REFERER']))

def default_resp(env, values, request, response):
    try:
        response.msg = response.msg.decode('utf-8')
    except UnicodeDecodeError:
        response.msg = response.msg.decode('iso8859-15')
    if response.headers.has_key('location'):
        print response.headers['location'][0]
        response.headers['location'][0] =  re.sub(r'http[s]*://' + env['target'].netloc,
                env['mandaye.scheme'] + '://' + env["HTTP_HOST"],
                response.headers['location'][0])
    response.msg = re.sub(r'http[s]*://' + env['target'].netloc,
            env['mandaye.scheme'] + '://' + env["HTTP_HOST"],
            response.msg)
    response.msg = response.msg.encode('utf-8')
    return response

def associate_req(env, values, request):
    session = env['beaker.session']
    if session.has_key('registraion') and \
            session['registraion'] == True:
        logger.info('Auto associate %s' % session['register_login'])
        headers = HTTPHeader()
        target = values['action']
        content = {values['login_name']: session['register_login'],
                values['password_name']: session['register_password']}
        content = urllib.urlencode(content)
        request = HTTPRequest(request.cookies,
                headers,
                "POST",
                content,
                target=values['action'])
    return request

def clean_registration_session(env, values, request, response):
    session = env['beaker.session']
    session['registraion'] = False
    if session.has_key('register_login'):
        del session['register_login']
    if session.has_key('register_password'):
        del session['register_password']
    session.save()
    return response

def associate(env, values, request, response):
    qs = parse_qs(env['QUERY_STRING'])
    if qs.has_key('type'):
        values['type'] = qs['type'][0]
    else:
        values['type'] = None
    return template_response(values.get('template'), values)

def associate_confirm(env, values, request, response):
    return template_response(values.get('template'), values)

def json_response(env, values, request, response):
    site_name = env["mandaye.config"]["site_name"]
    headers = HTTPHeader({'Content-Type': ['application/json']})
    target = '%s/DEFAULT/Ermes/Services/ILSClient.svc/RetrieveAccount' % \
            env['target'].geturl()
    auth = env['mandaye.auth']

    qs = parse_qs(env['QUERY_STRING'])
    if qs.has_key('nameid'):
        unique_id = qs['nameid'][-1]
    else:
        logger.warning('archimed json: no nameid id into get')
        return HTTPResponse(401, 'Unauthorized', headers,
                '{"error": "bad parameter no nameid"}')
    associations = Association.get(site_name, unique_id)
    if not associations:
        return HTTPResponse(401, 'Unauthorized', headers,
            '{"error": "%s is not associate with %s"}' %\
                (unique_id, site_name))
    association = associations[0]

    post_values = copy.copy(association['sp_post_values'])
    if config.encrypt_sp_password:
        password = auth.decrypt_pwd(post_values[auth.form_values['password_field']])
        post_values[auth.form_values['password_field']] = password
    response = auth.replay(env, post_values)
    cookies = response.cookies
    content = '{"codeConfig":"", "xslPath":"Services/LectorShortAccount.xslt"}'
    request = HTTPRequest(cookies, headers, "POST", content)
    request.msg = content
    return get_response(env, request, target)

def registration_req(env, values, request):
    session = env['beaker.session']
    if request.msg:
        post = request.msg.read()
        params = json.loads(post)
        if params.has_key('values'):
            if params['values'].has_key('_x002F_Registration_x002F_UserAccount_x005B_1_x005D__x002F_Login_x005B_1_x005D_'):
                session['register_login'] = params['values']['_x002F_Registration_x002F_UserAccount_x005B_1_x005D__x002F_Login_x005B_1_x005D_']
            if params['values'].has_key('_x002F_Registration_x002F_UserAccount_x005B_1_x005D__x002F_Password_x005B_1_x005D_'):
                session['register_password'] = params['values']['_x002F_Registration_x002F_UserAccount_x005B_1_x005D__x002F_Password_x005B_1_x005D_']
        request.msg = post
        session.save()
    return request

def registration_resp(env, values, request, response):
    session = env['beaker.session']
    if response.msg and \
            '"success":true' in response.msg and \
            session.has_key('register_login') and \
            session.has_key('register_password'):
        logger.info('Regastration completed for %s' % session['register_login'])
        session['registraion'] = True
    else:
        logger.info('Registration failed return original response')
        if session.has_key('register_login'):
            del session['register_login']
        if session.has_key('register_password'):
            del session['register_password']
        session['registraion'] = False
    session.save()
    return response

def registration_association(env, values, request, response):
    session = env['beaker.session']
    if session.has_key('registraion') and \
            session['registraion'] == True:
        return _302('/mandaye/associate_confirm')
    else:
        clean_registration_session(env, values, request, response)
        return response

def rewrite_logged_box(env, values, request, response):
    session = env.get('beaker.session')
    if response.msg and \
            'class="account_logoff"' in response.msg and \
            session and \
            session.has_key('unique_id'):
        r = re.compile(
                r'<a class="account_logoff" target="_top" href="#" tabindex="-1" role="menuitem".*?</a>',
                re.MULTILINE|re.DOTALL)

        resp = re.sub(r,
                r"""
<a target="_top" href="/mandaye/logout" tabindex="-1" role="menuitem"><i class="icon-off"> </i><span>Me déconnecter</span></a>
""",
                response.msg)
        response.msg = resp
    return response

def is_user_locally_logged_in(env, request, response):
    session = env['beaker.session']
    if session.has_key('locally_logged_in') and session['locally_logged_in']:
        return True
    return False

def is_logged_in(env, values, request, response):
    session = env['beaker.session']
    if response.msg and '<div id="identite_utilisateur">Bienvenue' in response.msg:
        session['locally_logged_in'] = True
    else:
        session['locally_logged_in'] = False
    session.save()
