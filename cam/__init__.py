__version__="0.3.1"

import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
default_config = os.path.join(BASE_DIR, 'default-config.ini')
