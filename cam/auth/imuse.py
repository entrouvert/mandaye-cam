
import hashlib
import urllib

from urlparse import parse_qs

from mandaye.auth.saml2 import SAML2Auth

class IMuseAuth(SAML2Auth):

    def associate_submit(self, env, values, request, response):
        if request.msg:
            if type(request.msg) == str:
                post = parse_qs(request.msg, request)
            else:
                post = request.msg.read()
                post = parse_qs(post, request)
            pwd_field = self.form_values['password_field']
            pwd = post[pwd_field][0]
            post[pwd_field][0] = hashlib.md5(pwd).hexdigest()
            post_values = {}
            for key, value in post.iteritems():
                post_values[key] = value[0]
            request.msg = urllib.urlencode(post_values)
        return super(IMuseAuth, self).associate_submit(env, values, request, response)

