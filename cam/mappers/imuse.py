
from cam.filters import imuse
from mandaye.filters.default import MandayeFilter

form_values = {
        'login_url': '/montpellier/extranet/login/usa_index_famille.php',
        'post_url': '/montpellier/extranet/login/gen_identification.php',
        'post_fields': ['login', 'mdp', 'site', 'groupe'],
        'username_field': 'login',
        'password_field': 'mdp',
        }

urls = {
        'login_url': '/mandaye/login',
        'connection_url': '/mandaye/sso',
        'associate_url': '/montpellier/extranet/login/mandaye_associate',
        'disassociate_url': '/mandaye/disassociate',
        'disassociate_next_url': '/mandaye/logout?next_url=/montpellier/extranet/login/usa_index_famille.php',
        'toolbar_logout_url': '/mandaye/logout?next_url=/montpellier/extranet/login/usa_index_famille.php',
        }

replay_condition = lambda env, response: "OK" in response.msg
is_user_locally_logged_in = imuse.is_user_locally_logged_in

mapping = [
        {
            'path': r'/$',
            'method': 'GET',
            'redirect': '/montpellier/extranet'
            },
        {
            'path': r'/mandaye/login$',
            'method': 'GET',
            'response': {
                'auth': 'login',
                'values': {
                    'associate_url': urls['associate_url'],
                    'next_url': '/montpellier/extranet/famille/usa_famille.php'
                    },
                }
            },
        {
            'path': r'/montpellier/extranet/login/gen_identification.php$',
            'method': 'POST',
            'on_request': [
                {'filter': MandayeFilter.store_request_content_buffer}
                ],
            'on_response': [{'auth': 'store_credentials_in_session'}]
            },
        {
            'path': r'/montpellier/extranet/login/mandaye_associate$',
            'method': 'GET',
            'response': {
                'filter': imuse.associate,
                },
            },
        {
                'path':  r'/montpellier/extranet/login/mandaye_associate$',
                'method': 'POST',
                'response': {
                    'auth': 'associate_submit',
                    },
                },
        {
                'path':  r'/montpellier/extranet/login/gen_logout.php$',
                'on_response': [{'auth': 'logout',}],
            },
        {
                'path':  r'/montpellier/extranet/.*.php$',
                'method': 'GET',
                'on_response': [{
                    'content-types': ['text/html'],
                    'filter': imuse.is_locally_logged_in,
                    }],
                },
]
