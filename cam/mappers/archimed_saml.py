
from mandaye.auth.saml2 import END_POINTS_PATH
from mandaye.filters.default import MandayeFilter

from cam.filters import archimed

base = 'DEFAULT'

form_values = {
        'login_url': '/%s' % base,
        'post_url': '/%s/Default/Ermes/Recherche/logon.svc/logon' % base,
        'post_fields': ['username', 'password'],
        'username_field': 'username',
        'password_field': 'password',
        }

replay_condition = lambda env, response: '"success":true' in response.msg
is_user_locally_logged_in = archimed.is_user_locally_logged_in

urls = {
        'login_url': '/mandaye/login',
        'logout_url': '/mandaye/logout',
        'connection_url': '/mandaye/sso',
        'associate_url': '/mandaye/associate',
        'disassociate_url': '/mandaye/disassociate',
        'disassociate_next_url': '/mandaye/logout',
        'toolbar_logout_url': '/mandaye/logout',
        }

mapping = [
        {
            'path': r'/',
            'on_request': [{
                'filter': archimed.default_req,
                'decompress': False,
                }]
            },
        {
            'path': r'/',
            'on_response': [{
                'filter': archimed.default_resp,
                'content-types': ['text/html', 'application/javascript', 'application/x-javascript', 'application/json'],
                }]
            },
        {
            'path': r'%s' % form_values['post_url'],
            'method': 'POST',
            'on_request': [
                {'filter': MandayeFilter.store_request_content_buffer}
                ],
            'on_response': [{'auth': 'store_credentials_in_session'}]
            },
        {
            'path': r'/',
            'method': 'GET',
            'content-types': ['text/html'],
            'on_response': [
                {
                    'filter': archimed.is_logged_in,
                    },
                {
                    'filter': archimed.rewrite_logged_box
                    },
                ],
            },
        {
            'path': r'/mandaye/login$',
            'method': 'GET',
            'response': {
                'auth': 'login',
                'values': {
                    'associate_url': '/mandaye/associate',
                    },
                }
            },
        {
            'path': r'/mandaye/sso$',
            'method': 'GET',
            'response': {'auth': 'sso',}
            },
        {
                'path': r'/%s/DEFAULT/Ermes/Recherche/logon.svc/logoff$' % base,
                'on_response': [{'auth': 'logout',}],
                },
        {
                'path': r'%s$' % END_POINTS_PATH['single_sign_on_post'],
                'method': 'POST',
                'response': {
                    'auth': 'single_sign_on_post',
                    'values': {
                        'login_url': '/mandaye/login',
                        'next_url': '/%s/' % base
                        }
                    }
                },
        {
                'path': r'%s$' % END_POINTS_PATH['single_logout'],
                'method': 'GET',
                'response': {'auth': 'single_logout'}
                },
        {
                'path': r'%s$' % END_POINTS_PATH['single_logout_return'],
                'method': 'GET',
                'response': {
                    'auth': 'single_logout_return',
                    'values': {
                        'next_url': '/%s' % base
                        }
                    }
                },
        {
                'path': r'/mandaye/associate$',
                'method': 'GET',
                'response': {
                    'filter': archimed.associate,
                    'values': {
                        'action': '/mandaye/associate?next_url=/%s' % base,
                        'template': 'archimed/associate.html',
                        'sp_name': 'Archimed',
                        'login_name': form_values['username_field'],
                        'password_name': form_values['password_field'],
                        },
                    },
                'on_request': [{
                    'filter': archimed.associate_req,
                    'values': {
                        'action': '/mandaye/associate?next_url=/%s' % base,
                        'login_name': form_values['username_field'],
                        'password_name': form_values['password_field'],
                        },
                    }]
                },
        {
                'path': r'/mandaye/associate_confirm$',
                'method': 'GET',
                'response': {
                    'filter': archimed.associate_confirm,
                    'values': {
                        'associate_url': '/mandaye/associate',
                        'template': 'archimed/associate_confirm.html',
                        },
                    }
                },
        {
                'path':  r'/mandaye/associate$',
                'method': 'POST',
                'response': {
                    'auth': 'associate_submit',
                    },
                'on_reponse': [
                    {
                        'filter': archimed.clean_registration_session,
                        },
                    ],
                },
        {
                'path': r'/mandaye/json$',
                'method': 'GET',
                'response': {'filter': archimed.json_response,},
                },
        ]
