#! /usr/bin/env python

'''
   Setup script for CAM RP
'''

import os
import subprocess

from setuptools import setup, find_packages
from sys import version

install_requires=[
          'gunicorn>=0.13',
          'mandaye>=0.8',
          'pycrypto>=2.0',
]

def get_version():
    if os.path.exists('VERSION'):
        version_file = open('VERSION', 'r')
        version = version_file.read()
        version_file.close()
        return version
    if os.path.exists('.git'):
        p = subprocess.Popen(['git', 'describe', '--match=v*'],
                stdout=subprocess.PIPE)
        result = p.communicate()[0]
        version = result.split()[0][1:]
        return version.replace('-','.')
    import cam
    return cam.__version__

setup(name="mandaye-cam",
      version=get_version(),
      license="AGPLv3 or later",
      description="CAM is a Mandaye project, modular reverse proxy to authenticate",
      url="http://dev.entrouvert.org/projects/reverse-proxy/",
      author="Entr'ouvert",
      author_email="info@entrouvert.org",
      maintainer="Jerome Schneider",
      maintainer_email="jschneider@entrouvert.com",
      scripts=['manager.py', 'server.py'],
      packages=find_packages(),
      include_package_data=True,
      install_requires=install_requires
)

